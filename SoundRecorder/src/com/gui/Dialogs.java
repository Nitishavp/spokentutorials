package com.gui;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.*;
import javax.swing.event.ListSelectionListener;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static javax.swing.ScrollPaneConstants.*;

public class Dialogs {// implements ActionListener {

	private static final Border border = BorderFactory.createLineBorder(Color.LIGHT_GRAY);
	private JFrame frame;
	protected JTextField textField;
    protected JTextArea textArea;
    protected JButton button;
    private JScrollPane scrollPane_2,scrollPane_3,scrollPane_4;
    //private JScrollPane scrollPane_1_1;
    private JLabel lblMandatoryHeadings;
    private JLabel lblHeadings;
    private JTextField textField_1;
    private JButton button_1;
    private JButton btnNewButton;
    private JButton button_2;
    private JLabel lblFontSize;
    private JLabel lblHeadings_1;
    private JLabel lblText;
    private JTextField textField_2;
    private JLabel lblOutput;
    private JButton btnCompile;
    private JList fontList,fontHeadingList,fontTextList;
    private JLabel lblFontFamily;
    private JLabel lblHeadings_2;
    private JTextArea textArea_1;
    private JLabel lblFontSize_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dialogs window = new Dialogs();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Dialogs() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 754, 516);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JLabel lblCompileText = new JLabel("Compile Text");
		lblCompileText.setFont(new Font("Tahoma", Font.PLAIN, 21));
		springLayout.putConstraint(SpringLayout.NORTH, lblCompileText, 38, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblCompileText, 34, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblCompileText);
		
		JLabel lblNewLabel = new JLabel("Headings");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel, 260, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel, 34, SpringLayout.WEST, frame.getContentPane());
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		frame.getContentPane().add(lblNewLabel);
		
		textField = new JTextField(20);
		springLayout.putConstraint(SpringLayout.WEST, textField, 0, SpringLayout.WEST, lblCompileText);
		frame.getContentPane().add(textField);
		button=new JButton("+");
		springLayout.putConstraint(SpringLayout.WEST, button, 2, SpringLayout.EAST, textField);
		springLayout.putConstraint(SpringLayout.SOUTH, button, -130, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, textField, 1, SpringLayout.NORTH, button);
        button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
        frame.getContentPane().add(button);
        
        
        
        
				        lblMandatoryHeadings = new JLabel("Mandatory Headings");
				        springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel, -15, SpringLayout.NORTH, lblMandatoryHeadings);
				        springLayout.putConstraint(SpringLayout.WEST, lblMandatoryHeadings, 35, SpringLayout.WEST, frame.getContentPane());
				        springLayout.putConstraint(SpringLayout.SOUTH, lblMandatoryHeadings, -6, SpringLayout.NORTH, textField);
		        springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel, -15, SpringLayout.NORTH, lblMandatoryHeadings);
		        lblMandatoryHeadings.setFont(new Font("Tahoma", Font.PLAIN, 15));
		        frame.getContentPane().add(lblMandatoryHeadings);
		        
		        lblHeadings = new JLabel(" Headings Not Recommended");
		        springLayout.putConstraint(SpringLayout.NORTH, lblHeadings, 0, SpringLayout.NORTH, lblMandatoryHeadings);
		        lblHeadings.setFont(new Font("Tahoma", Font.PLAIN, 15));
		        frame.getContentPane().add(lblHeadings);
		        
		        textField_1 = new JTextField(20);
		        springLayout.putConstraint(SpringLayout.WEST, textField_1, 64, SpringLayout.EAST, button);
		        springLayout.putConstraint(SpringLayout.WEST, lblHeadings, 0, SpringLayout.WEST, textField_1);
		        springLayout.putConstraint(SpringLayout.NORTH, textField_1, 0, SpringLayout.NORTH, textField);
		        frame.getContentPane().add(textField_1);
		        
		        button_1 = new JButton("+");
		        springLayout.putConstraint(SpringLayout.NORTH, button_1, -1, SpringLayout.NORTH, textField);
		        springLayout.putConstraint(SpringLayout.WEST, button_1, 4, SpringLayout.EAST, textField_1);
		        button_1.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        	}
		        });
		        frame.getContentPane().add(button_1);
		        
		        
		        

		        
		        button_2 = new JButton("Check");
		        springLayout.putConstraint(SpringLayout.NORTH, button_2, 0, SpringLayout.NORTH, btnNewButton);
		        springLayout.putConstraint(SpringLayout.WEST, button_2, 0, SpringLayout.WEST, lblHeadings);
		        button_2.setFont(new Font("Tahoma", Font.PLAIN, 9));
		        frame.getContentPane().add(button_2);
		        
		        lblFontSize = new JLabel("Standardize Font");
		        springLayout.putConstraint(SpringLayout.NORTH, lblFontSize, 49, SpringLayout.SOUTH, lblCompileText);
		        springLayout.putConstraint(SpringLayout.WEST, lblFontSize, 0, SpringLayout.WEST, lblCompileText);
		        lblFontSize.setFont(new Font("Tahoma", Font.PLAIN, 18));
		        frame.getContentPane().add(lblFontSize);
		        
		        lblHeadings_1 = new JLabel("FontSize");
		        springLayout.putConstraint(SpringLayout.NORTH, lblHeadings_1, 44, SpringLayout.SOUTH, lblFontSize);
		        springLayout.putConstraint(SpringLayout.WEST, lblHeadings_1, 0, SpringLayout.WEST, lblCompileText);
		        springLayout.putConstraint(SpringLayout.SOUTH, lblHeadings_1, -355, SpringLayout.SOUTH, frame.getContentPane());
		        lblHeadings_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		        frame.getContentPane().add(lblHeadings_1);
		        
		        lblText = new JLabel("Text");
		        springLayout.putConstraint(SpringLayout.WEST, lblText, 136, SpringLayout.WEST, frame.getContentPane());
		        lblText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		        frame.getContentPane().add(lblText);
		        
		        textField_2 = new JTextField();
		        textField_2.setEditable(false);
		        frame.getContentPane().add(textField_2);
		        textField_2.setColumns(10);
		        
		        lblOutput = new JLabel("Output");
		        springLayout.putConstraint(SpringLayout.NORTH, textField_2, 7, SpringLayout.SOUTH, lblOutput);
		        springLayout.putConstraint(SpringLayout.WEST, textField_2, 0, SpringLayout.WEST, lblOutput);
		        springLayout.putConstraint(SpringLayout.NORTH, lblOutput, 64, SpringLayout.NORTH, frame.getContentPane());
		        springLayout.putConstraint(SpringLayout.EAST, lblOutput, -318, SpringLayout.EAST, frame.getContentPane());
		        lblOutput.setFont(new Font("Tahoma", Font.PLAIN, 18));
		        frame.getContentPane().add(lblOutput);
		        
		        btnCompile = new JButton("Compile");
		        springLayout.putConstraint(SpringLayout.EAST, lblNewLabel, -442, SpringLayout.WEST, btnCompile);
		        springLayout.putConstraint(SpringLayout.NORTH, btnCompile, 259, SpringLayout.NORTH, frame.getContentPane());
		        springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel, 1, SpringLayout.NORTH, btnCompile);
		        springLayout.putConstraint(SpringLayout.SOUTH, textField_2, -6, SpringLayout.NORTH, btnCompile);
		        springLayout.putConstraint(SpringLayout.EAST, textField_2, 0, SpringLayout.EAST, btnCompile);
		        springLayout.putConstraint(SpringLayout.EAST, btnCompile, -27, SpringLayout.EAST, frame.getContentPane());
		        btnCompile.setFont(new Font("Tahoma", Font.PLAIN, 15));
		        frame.getContentPane().add(btnCompile);
			          
			                  //textArea.setEditable(false);
			          
		
		
			          textArea = new JTextArea(5, 20);
			          //springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 6, SpringLayout.SOUTH, textArea);
			          frame.getContentPane().add(textArea);
			          //springLayout.putConstraint(SpringLayout.NORTH, textArea, 6, SpringLayout.SOUTH, textField);
			          //springLayout.putConstraint(SpringLayout.WEST, textArea, 0, SpringLayout.WEST, lblCompileText);
			          textArea.setTabSize(3);
			          textArea.setBorder(border);
			          
				        /*btnNewButton = new JButton("Check");
				        springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 0, SpringLayout.WEST, lblCompileText);
				        
				        springLayout.putConstraint(SpringLayout.SOUTH, textArea, -6, SpringLayout.NORTH, btnNewButton);
				        btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 9));
				        frame.getContentPane().add(btnNewButton);
				        */
			            textArea_1 = new JTextArea(5, 20);
			            springLayout.putConstraint(SpringLayout.SOUTH, textArea_1, 0, SpringLayout.SOUTH, textArea);
			            springLayout.putConstraint(SpringLayout.EAST, textArea_1, 0, SpringLayout.EAST, textField_1);
			            textArea_1.setTabSize(3);
			            textArea_1.setBorder(border);
			            frame.getContentPane().add(textArea_1);
		        String fonts[] = 
		        	      GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		        String fontSize[]={"1","2","3","4","5","6","7","8","9","10","11","12","14","16","18","20","22","24","26","28","36","48","72"};
		        fontList = new JList(fonts);
		        fontList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		        fontList.setValueIsAdjusting(true);
		        fontHeadingList = new JList(fontSize);
		        fontTextList=new JList(fontSize);

		        fontList.setVisibleRowCount(1);
		        fontList.addListSelectionListener(new ListSelectionListener()
		        {
		    		
		    		@Override
		    		public void valueChanged(ListSelectionEvent e) {
		    			String fontname=fontList.getSelectedValue().toString();
		    			
		    		}
		        });
		        fontHeadingList.setVisibleRowCount(1);
		        fontHeadingList.setSelectedIndex(14);
		        fontHeadingList.addListSelectionListener(new ListSelectionListener()
		        {
		    		
		    		@Override
		    		public void valueChanged(ListSelectionEvent e) {
		    			String headingsize=fontHeadingList.getSelectedValue().toString();
		    			
		    		}
		        });
		        fontTextList.setVisibleRowCount(1);
		        fontTextList.addListSelectionListener(new ListSelectionListener()
		        {
		    		
		    		@Override
		    		public void valueChanged(ListSelectionEvent e) {
		    			String textsize=fontTextList.getSelectedValue().toString();
		    			
		    		}
		        });
		        scrollPane_2 = new JScrollPane(fontList);
		        springLayout.putConstraint(SpringLayout.NORTH, scrollPane_2, 18, SpringLayout.SOUTH, lblFontSize);
		        springLayout.putConstraint(SpringLayout.WEST, scrollPane_2, 0, SpringLayout.WEST, lblText);
		        springLayout.putConstraint(SpringLayout.EAST, scrollPane_2, -412, SpringLayout.EAST, frame.getContentPane());
		        springLayout.putConstraint(SpringLayout.WEST, textField_2, 22, SpringLayout.EAST, scrollPane_2);
		        scrollPane_2.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
		        scrollPane_3 = new JScrollPane(fontHeadingList);
		        springLayout.putConstraint(SpringLayout.WEST, scrollPane_3, 0, SpringLayout.WEST, button);
		        scrollPane_4 = new JScrollPane(fontTextList);
		        springLayout.putConstraint(SpringLayout.SOUTH, scrollPane_3, -6, SpringLayout.NORTH, scrollPane_4);
		        springLayout.putConstraint(SpringLayout.NORTH, scrollPane_4, 220, SpringLayout.NORTH, frame.getContentPane());
		        springLayout.putConstraint(SpringLayout.WEST, scrollPane_4, 0, SpringLayout.WEST, button);
		        frame.getContentPane().add(scrollPane_2);
		        frame.getContentPane().add(scrollPane_3);
		        frame.getContentPane().add(scrollPane_4); 
		        
		        lblFontFamily = new JLabel("Font Family");
		        springLayout.putConstraint(SpringLayout.NORTH, scrollPane_2, 6, SpringLayout.NORTH, lblFontFamily);
		        springLayout.putConstraint(SpringLayout.SOUTH, scrollPane_2, 25, SpringLayout.NORTH, lblFontFamily);
		        springLayout.putConstraint(SpringLayout.NORTH, lblFontFamily, 21, SpringLayout.SOUTH, lblFontSize);
		        springLayout.putConstraint(SpringLayout.SOUTH, lblFontFamily, -295, SpringLayout.SOUTH, frame.getContentPane());
		        springLayout.putConstraint(SpringLayout.EAST, lblFontFamily, 83, SpringLayout.WEST, lblCompileText);
		        springLayout.putConstraint(SpringLayout.WEST, lblFontFamily, 0, SpringLayout.WEST, lblCompileText);
		        lblFontFamily.setFont(new Font("Tahoma", Font.PLAIN, 15));
		        frame.getContentPane().add(lblFontFamily); 
		        
		        lblHeadings_2 = new JLabel("Headings");
		        springLayout.putConstraint(SpringLayout.NORTH, scrollPane_3, -2, SpringLayout.NORTH, lblHeadings_2);
		        springLayout.putConstraint(SpringLayout.NORTH, lblHeadings_2, 15, SpringLayout.SOUTH, scrollPane_2);
		        springLayout.putConstraint(SpringLayout.WEST, lblHeadings_2, 136, SpringLayout.WEST, frame.getContentPane());
		        springLayout.putConstraint(SpringLayout.NORTH, lblText, 13, SpringLayout.SOUTH, lblHeadings_2);
		        springLayout.putConstraint(SpringLayout.NORTH, lblText, 13, SpringLayout.SOUTH, lblHeadings_2);
		        springLayout.putConstraint(SpringLayout.WEST, lblText, 0, SpringLayout.WEST, lblHeadings_2);
		        lblHeadings_2.setFont(new Font("Tahoma", Font.PLAIN, 11));
		        frame.getContentPane().add(lblHeadings_2);
		        
		        lblFontSize_1 = new JLabel("Font Size");
		        springLayout.putConstraint(SpringLayout.NORTH, lblFontSize_1, 0, SpringLayout.NORTH, lblHeadings_2);
		        springLayout.putConstraint(SpringLayout.WEST, lblFontSize_1, 0, SpringLayout.WEST, lblCompileText);
		        lblFontSize_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		        frame.getContentPane().add(lblFontSize_1);
	}

}
