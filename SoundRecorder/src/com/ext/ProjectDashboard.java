package com.ext;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;

public class ProjectDashboard {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProjectDashboard window = new ProjectDashboard();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ProjectDashboard() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 367, 302);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JLabel lblNewLabel = new JLabel("Project Dashboard");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel, 34, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel, 25, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Add Presentation");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 13));
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 39, SpringLayout.SOUTH, lblNewLabel);
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 27, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(btnNewButton);
		
		JButton btnAddScreenRecording = new JButton("Add Screen Recording");
		springLayout.putConstraint(SpringLayout.NORTH, btnAddScreenRecording, 28, SpringLayout.SOUTH, btnNewButton);
		springLayout.putConstraint(SpringLayout.WEST, btnAddScreenRecording, 0, SpringLayout.WEST, lblNewLabel);
		btnAddScreenRecording.setFont(new Font("Tahoma", Font.PLAIN, 13));
		frame.getContentPane().add(btnAddScreenRecording);
		
		JButton btnExportToVideo = new JButton("Export to Video");
		springLayout.putConstraint(SpringLayout.NORTH, btnExportToVideo, 32, SpringLayout.SOUTH, btnAddScreenRecording);
		springLayout.putConstraint(SpringLayout.WEST, btnExportToVideo, 0, SpringLayout.WEST, lblNewLabel);
		btnExportToVideo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		frame.getContentPane().add(btnExportToVideo);
	}

}
