/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spokentutorials;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Joey
 */
public class Slide {
    private String name,type;
    private Icon icon;
    private int order;
    
    public String getName(){
        return name;
    }
    public String getType(){
        return type;
    }
    public Icon getIcon(){
        return icon;
    }
    public int getOrder(){
        return order;
    }
    public void setName(String name){
        this.name=name;
    }
    public void setType(String type){
        this.type=type;
    }
    public void  setIcon(Icon icon){
        this.icon=icon;
    }
    public void setOrder(int order){
        this.order=order;
    }
    public ImageIcon getThumbnail(){
      ImageIcon img=(ImageIcon)icon;
      Image scaleImage = img.getImage().getScaledInstance(100, 100,Image.SCALE_DEFAULT);
     return new ImageIcon(scaleImage);
    }
}
