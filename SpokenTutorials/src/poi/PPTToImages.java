package poi;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.apache.poi.xslf.usermodel.*;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.sl.usermodel.SlideShow;
//import org.apache.commons.lang.*;

public class PPTToImages {
	public String path,location;

	public void getImages()
	{
		try
		{
			File file=new File(path);
		      XMLSlideShow ppt = new XMLSlideShow(new FileInputStream(file));
		      
		      //getting the dimensions and size of the slide 
		      Dimension pgsize = ppt.getPageSize();
		      List<XSLFSlide> slide = ppt.getSlides();
		      
		      for (int i = 0; i < slide.size(); i++) {
		         BufferedImage img = new BufferedImage(pgsize.width, pgsize.height,BufferedImage.TYPE_INT_RGB);
		         Graphics2D graphics = img.createGraphics();
	
		         //clear the drawing area
		         graphics.setPaint(Color.white);
		         graphics.fill(new Rectangle2D.Float(0, 0, pgsize.width, pgsize.height));
	
		         //render
		         slide.get(i).draw(graphics);
		         
		       //creating an image file as output
		         //String projectPath=new File(path).getParentFile().getAbsolutePath();
		         String fileName=(Integer.toString(i+1))+".jpg";
		         fileName=new File(location,fileName).getAbsolutePath();
		         FileOutputStream out = new FileOutputStream(fileName);
		         javax.imageio.ImageIO.write(img, "jpg", out);
		         ppt.write(out);
		         out.close();
		      }
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


	public PPTToImages(String presentation)
	{
        String[] subDirs = new String[10];
        presentation=new File(presentation).getAbsolutePath();
        String filename=new File(presentation).getName();
            
                location=presentation.substring(0, presentation.lastIndexOf("\\"));
            
		System.out.println(location);
		System.out.println(filename);
		path=new File(location,filename).getAbsolutePath();
        System.out.println(path);
                
	}
        public static void main(String[] args) {
        new PPTToImages("C:\\Users\\Joey\\Documents\\ppt\\Transforming Lucknow.pptx").getImages();
    }
}