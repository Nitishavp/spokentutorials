/**************************************************************
 * 
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * 
 *************************************************************/
/*
 * OfficeUNOClientApp.java
 *
 * Created on 2015.12.28 - 12:15:02
 *
 */

package com.example;

import com.sun.star.comp.helper.Bootstrap;
import com.sun.star.lang.XComponent;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import ooo.connector.BootstrapSocketConnector;

/**
 *
 * @author Joey
 */
public class OfficeUNOClientApp1 {
    
    /** Creates a new instance of OfficeUNOClientApp_Desktop */
    public OfficeUNOClientApp1() {
    }
    //Method definitions
    /*public static string showfilesaver()
	{
                String file="";
		JFileChooser fileChooser = new JFileChooser();
		if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                    file = fileChooser.getSelectedFile().getName();
		  System.out.println(file);
		}
                return file;
	}*/
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("1");    
        try {
            // get the remote office component context
            System.out.println("2");
            //String oooExeFolder = "C:/Program Files (x86)/OpenOffice 4/program";
            String oooExeFolder = "C:/Program Files (x86)/LibreOffice 5/program";
            //XComponentContext xContext = Bootstrap.bootstrap();
            XComponentContext xContext = BootstrapSocketConnector.bootstrap(oooExeFolder);
            System.out.println("3");
            if (xContext == null) {
                System.err.println("ERROR: Could not bootstrap default Office.");
                
            }
            else{
                System.out.println("xContent not null");
            }    
            com.sun.star.lang.XMultiComponentFactory xMCF = xContext.getServiceManager();
            Object oDesktop = xMCF.createInstanceWithContext("com.sun.star.frame.Desktop", xContext);
            
            com.sun.star.frame.XComponentLoader xCompLoader =
                UnoRuntime.queryInterface(
                 com.sun.star.frame.XComponentLoader.class, oDesktop);
            
            //XComponent document = xCompLoader.loadComponentFromURL("private:factory/simpress", "_blank", 0, new com.sun.star.beans.PropertyValue[0]);
            XComponent document = xCompLoader.loadComponentFromURL("file:///C:/Users/Joey/Desktop/joey.odp", "_blank", 0, new com.sun.star.beans.PropertyValue[0]);
            // Get the textdocument
            /*XTextDocument aTextDocument = ( XTextDocument )UnoRuntime.queryInterface(com.sun.star.text.XTextDocument.class, document);
            
            // Get its text
            XText xText = aTextDocument.getText();*/
            //String URL=showfilesaver();
         /*   
            try {
                Thread.sleep(7000);                 //1000 milliseconds is one second.
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
           */ 
            
            String SaveURL="file:///C:/Users/Joey/Desktop/test.odp";
         
           com.sun.star.frame.XStorable xStorable =
                (com.sun.star.frame.XStorable)UnoRuntime.queryInterface(
                    com.sun.star.frame.XStorable.class, document ); 
           com.sun.star.beans.PropertyValue propertyValue[] = new com.sun.star.beans.PropertyValue[ 2 ];
           propertyValue[0] = new com.sun.star.beans.PropertyValue();
            propertyValue[0].Name = "Overwrite";
            propertyValue[0].Value = new Boolean(true);
            propertyValue[1] = new com.sun.star.beans.PropertyValue();
            propertyValue[1].Name = "FilterName";
            // propertyValue[1].Value = "StarOffice XML (Impress)";
           propertyValue[1].Value = "MS PowerPoint 97 Vorlage";
            xStorable.storeAsURL(SaveURL , propertyValue );
            
            System.out.println("Hello");
        }
        catch (java.lang.Exception e){
            e.printStackTrace();
        }
        finally {
            System.exit( 0 );
        }
    }
    
}
