/**************************************************************
 * 
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * 
 *************************************************************/
/*
 * OfficeUNOClientApp.java
 *
 * Created on 2015.12.28 - 12:15:02
 *
 */

package com.example;

import com.sun.star.uno.XComponentContext;
import com.sun.star.comp.helper.Bootstrap;

/**
 *
 * @author Joey
 */
public class OfficeUNOClientApp {
    
    /** Creates a new instance of OfficeUNOClientApp */
    public OfficeUNOClientApp() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // get the remote office component context
            XComponentContext xContext = Bootstrap.bootstrap();
            if (xContext == null) {
                System.err.println("ERROR: Could not bootstrap default Office.");
            }
            else System.out.println("executed");
        }
        catch (java.lang.Exception e){
            e.printStackTrace();
        }
        finally {
            System.exit( 0 );
        }
    }
    public void dispatch( com.sun.star.util.URL aURL,
                           com.sun.star.beans.PropertyValue[] aArguments )
    {
         if ( aURL.Protocol.compareTo("com.yourdomain.yourextension:") == 0 )
        {
            if ( aURL.Path.compareTo("MenuItem1") == 0 )
            {
                // add your own code here
                return;
            }
            if ( aURL.Path.compareTo("MenuItem2") == 0 )
            {
                // add your own code here
                return;
            }
        }
    }
}
